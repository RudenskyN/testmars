import java.util.ArrayList;


abstract class GpsTracking {
	
	double latitude;
	double longitude;
	ArrayList listCoord;
	
	public GpsTracking(){}
	
	protected void getThisCoordinats(){}
	
	protected void sendRequest(double latitude,	double longitude){}
	
	protected void parseData(String arg0){}
	
	protected void loadMap(){}
	
	protected void setMarkersOnMap(ArrayList list) {	
	}
	
}

